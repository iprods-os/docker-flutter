#!/usr/bin/env sh

set -euo pipefail

push=${1:-no-push}
build=${2:-all}
image_name=${CI_REGISTRY_IMAGE:-local/flutter}
last_version="n/a"

if [ "${push}" != "no-push" ]; then
  echo "Push when done enabled"
fi

# pull to make sure we are not rebuilding for nothing
docker build --tag ${image_name}:base \
             --pull \
             base
docker push ${image_name}:base

if [ "${build}" == "all" ]; then
  while IFS="" read -r version || [ -n "${version}" ]
  do
    docker build --tag ${image_name}:${version} \
                --pull \
                --build-arg IMAGE_NAME=${image_name} \
                --build-arg flutter_version=${version} \
                sdk
    if [ "${push}" == "push" ]; then
      docker push ${image_name}:${version}
    fi
    last_version=${version}
  done < flutter_versions
else
  latest="n/a"
  while IFS="" read -r version || [ -n "${version}" ]
  do
    latest=${version}
  done < flutter_versions
  docker build --tag ${image_name}:${latest} \
              --pull \
              --build-arg IMAGE_NAME=${image_name} \
              --build-arg flutter_version=${latest} \
              sdk
  if [ "${push}" == "push" ]; then
    docker push ${image_name}:${latest}
  fi
fi

if [ "${last_version}" != "n/a" ]; then
  echo "Last build ${last_version} will be tagged latest"
  docker build --tag ${image_name}:latest \
               --pull \
               --build-arg IMAGE_NAME=${image_name} \
               --build-arg flutter_version=${last_version} \
               sdk
  if [ "${push}" == "push" ]; then
    docker push ${image_name}:latest
  fi
fi


